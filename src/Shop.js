import React, { useEffect, useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const OrderState = ["ORDERED", "PREPARING", "DELIVERING", "DELIVERED", "COMPLETED", "CANCELLED"];
const UserType = ["BUYER", "RIDER", "SHOP"];

function Shop({contracts, accounts}) {
  const [shopInfos, setShopInfos] = useState([]);
  const [currentShop, setCurrentShop] = useState(undefined);
  const [openRegister, setOpenRegister] = useState(false);
  const [orders, setOrders] = useState([]);
  const [finishedOrders, setFinishedOrders] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [openUploadReview, setOpenUploadReview] = useState({
    open: false, 
    orderId: 0,
    reviewedToBuyer: false,
    reviewedToRider: false});

  const getShopInfo = async (_account) => {
    const shopIds = await contracts.user.methods
      .getUserShopIds(_account)
      .call();
    if(shopIds.length === 0)  return;

    let shopInfoTemp = [];
    for(let i = 0; i < shopIds.length; i++) {
      const shopId = shopIds[i];
      const phoneNumber = await contracts.shop.methods
        .getShopPhoneNumber(shopId)
        .call();
      const reviews = await contracts.shop.methods
        .getShopReviews(shopId)
        .call();
      const orders = await contracts.shop.methods
        .getShopOrders(shopId)
        .call();
      const shopAddress = await contracts.shop.methods
        .getShopAddress(shopId)
        .call();
      const activation = await contracts.shop.methods
        .getShopActivation(shopId)
        .call();
      
      shopInfoTemp.push({
        id: shopId,
        phoneNumber: phoneNumber,
        reviews: reviews,
        orders: orders,
        address: shopAddress,
        activation: activation });
    }

    setShopInfos(shopInfoTemp);
  }

  const getOrders = async (_orderIds) => {
    let ordersTemp = [];
    let finishedOrdersTemp = [];
    for(let i = 0; i < _orderIds.length; i++) {
      const currentState = await contracts.order.methods
      .getOrderState(_orderIds[i])
      .call();
      if(currentState == 5) continue;

      const buyer = await contracts.order.methods
        .getOrderBuyer(_orderIds[i])
        .call();
      const riderId = await contracts.order.methods
        .getOrderRiderId(_orderIds[i])
        .call();
      const totalPrice = await contracts.order.methods
        .getOrderTotalPrice(_orderIds[i])
        .call();
      const goodsIds = await contracts.order.methods
        .getOrderGoodsIds(_orderIds[i])
        .call();
      const stateTimes = await contracts.order.methods
        .getOrderStateTimes(_orderIds[i])
        .call();
      const cancelSign = await contracts.order.methods
        .getOrderCancelSign(_orderIds[i])
        .call();

      currentState != 0 && currentState != 1 ? (
        finishedOrdersTemp.push({
          orderId: _orderIds[i],
          buyer,
          riderId,
          totalPrice,
          goodsIds,
          stateTimes,
          currentState,
          cancelSign })      
      ) : (
        ordersTemp.push({
          orderId: _orderIds[i],
          buyer,
          riderId,
          totalPrice,
          goodsIds,
          stateTimes,
          currentState,
          cancelSign })
      )
    }
    
    setFinishedOrders(finishedOrdersTemp);
    setOrders(ordersTemp);
  }

  const getReviews = async (_reviewIds) => {
    let reviewsTemp = [];
    for(let i = 0; i < _reviewIds.length; i++) {
      const uploaderType = await contracts.review.methods
        .getReviewFromUserType(_reviewIds[i])
        .call();
      const orderId = await contracts.review.methods
        .getReviewOrderId(_reviewIds[i])
        .call();
      const stars = await contracts.review.methods
        .getReviewStars(_reviewIds[i])
        .call();
      const cancelled = await contracts.review.methods
        .getReviewCancelled(_reviewIds[i])
        .call();
      
      reviewsTemp.push({
        uploaderType,
        orderId,
        stars,
        cancelled });
    }

    setReviews(reviewsTemp);
  }

  const changeShopIdHandle = async (_e) => {
    if(!_e.target.value) return;

    const shopInfo = shopInfos[_e.target.value];
    await getOrders(shopInfo.orders)
    await getReviews(shopInfo.reviews);
    setCurrentShop(shopInfo);
  }

  const clickOpenRegisterHandle = () => { setOpenRegister(true); }

  const clickCloseRegisterHandle = () => { setOpenRegister(false); }

  const clickRegisterShopHande = async (_e) => {
    _e.preventDefault();

    const phoneNumber = document.getElementById("registerPhoneNumInput").value;
    const address = document.getElementById("registerAddressSelect").value;

    await contracts.shop.methods
      .registerShop(phoneNumber, address)
      .send({from: accounts[0]});

    await getShopInfo(accounts[0]);

    setOpenRegister(false);
  }

  const clickSignOrderHandle = async (_orderId) => {
    try{
      await contracts.order.methods
        .signOrder(_orderId)
        .send({from: accounts[0]});

      await getOrders(currentShop.orders);
    } catch(e) {
      alert(e);
    }
  }

  const clickCancelOrderHandle = async (_orderId, _idx) => {
    try{
      await contracts.order.methods
        .cancelOrderSign(_orderId)
        .send({from: accounts[0]});
      
      // const orderState = contracts.order.methods
      //   .getOrderState(_orderId)
      //   .call();
      // const cancelSign = contracts.order.methods
      //   .getOrderCancelSign(_orderId)
      //   .call();

      // let ordersCopy = orders;
      // ordersCopy[_idx] = {...ordersCopy[_idx], orderState, cancelSign}
      // console.log(ordersCopy[_idx]);

      // setOrders(ordersCopy);
      getOrders(currentShop.orders);
    } catch(e) {
      alert(e);
    }
  }

  const clickOpenUploadReviewHandle = async (_orderId) => { 
    const reviewedToBuyer = await contracts.review.methods
      .getReviewed(_orderId, 2, 0)
      .call();
    const reviewedToRider = await contracts.review.methods
      .getReviewed(_orderId, 2, 1)
      .call();

    setOpenUploadReview({
      open: true, 
      orderId: _orderId, 
      reviewedToBuyer, 
      reviewedToRider}); 
  }

  const clickUploadReviewHandle = async () => {
    const target = document.getElementById("selectReviewTarget").value;
    const score = document.getElementById("selectReviewScore").value;

    console.log(target, score, openUploadReview.orderId);

    try {
      if(score < 1 || score > 10 || target < 0 || target > 2) throw "invalid value";

      await contracts.review.methods
        .uploadReview(target, openUploadReview.orderId, score)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getReviews(currentShop.orders);
    setOpenUploadReview({open: false, orderId: 0});
  }

  const clickCloseUploadReviewHandle = () => { 
    setOpenUploadReview({
      open: false, 
      orderId: 0,
      reviewedToBuyer: false,
      reviewedToRider: false }); 
  }

  useEffect(() => {
    const init = async () => {
      await getShopInfo(accounts[0]);
      setCurrentShop(undefined);
      setOrders([]);
      setReviews([]);
    };
    init();
  }, [accounts[0]]);
  
  return (
    <main className="container-fluid row mt-md-3">

      <div className="col-sm-3 first-col">
        <div className="row form-group">
          <div>
            <select 
              className="custom-select"
              onChange={e => changeShopIdHandle(e)}>
              <option value="">Select Shop</option>
              {
                shopInfos.map((shopInfo, idx) => (
                  <option key={idx} value={idx}>{shopInfo.id}</option>
                ))
              }
            </select>
          </div>
          <button className="btn btn-primary btn-sm" onClick={clickOpenRegisterHandle}>Create Shop</button>
          <Modal isOpen={openRegister}>
            <ModalHeader>Register Shop</ModalHeader>
            <ModalBody>
              <div className="form-group">
                <label>phone number</label>
                <input
                  id="registerPhoneNumInput"
                  type="number"
                  className="form-control"
                  placeholder="phone number"
                ></input>
              </div>
              <div className="form-group">
                <label>Shop Address</label>
                <select className="form-control" id="registerAddressSelect">
                  <option style={{fontWeight: "bold"}} value="">Choose Address</option>
                  <option value="area1">Area1</option>
                  <option value="area2">Area2</option>
                </select>
              </div>
            </ModalBody>
            <ModalFooter>
              <button className="btn btn-primary" onClick={clickRegisterShopHande}>Register</button> 
              <button className="btn btn-secondary" onClick={clickCloseRegisterHandle}>Close</button>
            </ModalFooter>
          </Modal>
        </div>

        <div className="card border-light" style={{maxWidth: "30rem"}}>
          <div className="card-header">Shop Info</div>
          <div>
            { currentShop ? (
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Phone Number</th>
                  <td>{currentShop.phoneNumber}</td>
                </tr>
                <tr>
                  <th scope="row">Shop Address</th>
                  <td>{currentShop.address}</td>
                </tr>
                <tr>
                  <th scope="row">Activation</th>
                  <td>{String(currentShop.activation)}</td>
                </tr>
              </tbody>
            </table> ) : (
              <p>No selected Shop</p> )
            }
          </div>
        </div>
      </div>

      <div className="col-md-8" style={{textAlign: "center"}}>
        <div>
          <legend>Ongoing Orders</legend>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Order ID</th>
                <th scope="col">Buyer</th>
                <th scope="col">Rider</th>
                <th scope="col">Total Price</th> 
                <th scope="col">Goods</th>
                <th scope="col">Time</th>
                <th scope="col">State</th>
              </tr>
            </thead>
            <tbody>
              {
              orders.map((order, idx) => (
              <tr key={idx}>
                <td>{order.orderId}</td>
                <td>{order.buyer.substr(0, 10)}...</td>
                <td>{parseInt(order.riderId) || "not assigned"}</td>
                <td>{order.totalPrice}</td>
                <td>{String(order.goodsIds)}</td>
                <td>{new Date(order.stateTimes[0] * 1000).toLocaleString()}</td>
                <td>{OrderState[order.currentState]}</td>
                <td><button 
                      className="btn btn-primary btn-sm"
                      disabled={order.currentState == '0' ? false : true}
                      onClick={() => clickSignOrderHandle(order.orderId, idx)}
                >sign</button></td>
                <td>
                {
                order.cancelSign[1] ? (<>Signed</>) : (
                  <button
                    className="btn btn-danger btn-sm"
                    disabled={order.currentState == '0' || order.currentState == '1' ? false : true}
                    onClick={() => clickCancelOrderHandle(order.orderId, idx)}
                  >cancel</button> )
                }
                </td>
              </tr> ))
              }
            </tbody>
          </table>
        </div>
        <div>
          <legend>Finished Orders</legend>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Order ID</th>
                <th scope="col">Buyer</th>
                <th scope="col">Rider</th>
                <th scope="col">Total Price</th> 
                <th scope="col">Goods</th>
                <th scope="col">Time</th>
                <th scope="col">State</th>
              </tr>
            </thead>
            <tbody>
            {
            finishedOrders.map((order, idx) => (
              <tr key={idx}>
                <td>{order.orderId}</td>
                <td>{order.buyer.substr(0, 10)}...</td>
                <td>{order.riderId}</td>
                <td>{order.totalPrice}</td>
                <td>{String(order.goodsIds)}</td>
                <td>{new Date(order.stateTimes[0] * 1000).toLocaleString()}</td>
                <td>{OrderState[order.currentState]}</td>
                {order.currentState == 4 && 
                  (<td><button className="btn btn-primary btn-sm" onClick={() => clickOpenUploadReviewHandle(order.orderId)}>Upload Review</button></td>)}
              </tr>
            ))
            }
            </tbody>
          </table>
          <Modal isOpen={openUploadReview.open}>
            <ModalHeader>Upload Review</ModalHeader>
            <ModalBody>
              <div>
                <legend>To</legend>
                <select className="form-control" id="selectReviewTarget">
                  <option style={{fontWeight: "bold"}} value={-1}>Choose Target User</option>
                  <option value={0} disabled={openUploadReview.reviewedToBuyer}>Buyer</option>
                  <option value={1} disabled={openUploadReview.reviewedToRider}>Rider</option>
                </select>
              </div>
              <div>
                <legend>Score</legend>
                <select className="form-control" id="selectReviewScore">
                  <option style={{fontWeight: "bold"}} value={-1}>Choose Score</option>
                  {
                  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((score, idx) => (<option key={idx} value={score}>{score}</option>))
                  }
                </select>
              </div>
            </ModalBody>
            <ModalFooter>
              {
              openUploadReview.reviewedToRider && openUploadReview.reviewedToBuyer ? (
                <>You Already Uploaded Reviews</>
              ) : (
              <button className="btn btn-primary" onClick={() => clickUploadReviewHandle()}>Upload</button> )
              }
              <button className="btn btn-secondary" onClick={() => clickCloseUploadReviewHandle()}>Close</button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <legend>Reviews</legend>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Reviewer</th>
                <th scope="col">Order Id</th>
                <th scope="col">Score</th> 
              </tr>
            </thead>
            <tbody>
              {
                reviews.map((review, idx) => (
                <tr key={idx}>
                  <td>{UserType[review.uploaderType]}</td>
                  <td>{review.orderId}</td>
                  <td>{review.stars}</td>
                  <td><button className="btn btn-primary">Details</button></td>
                </tr> ))
              }
            </tbody>
          </table>
        </div>
      </div>

    </main>
  )
}

export default Shop;
