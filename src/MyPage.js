import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, UncontrolledCollapse } from 'reactstrap';

const OrderState = ["ORDERED", "PREPARING", "DELIVERING", "DELIVERED", "COMPLETED", "CANCELLED"];
const UserType = ["BUYER", "RIDER", "SHOP"];

function MyPage({contracts, userInfo, accounts}) {
  const [userOrders, setUserOrders] = useState([]);
  const [uploadedReviews, setUploadedReviews] = useState([]);
  const [openShopReviews, setOpenShopReviews] = useState({open: false, reviews: []});
  const [openUploadReview, setOpenUploadReview] = useState({
    open: false, 
    orderId: 0, 
    reviewedToRider: false,
    reviewedToShop: false});

  const getOrders = async (_account) => {
    const orderIds = await contracts.user.methods
      .getUserOrders(_account)
      .call();
    
    let ordersCopy = [];
    for(let i = 0; i < orderIds.length; i++) {
      const orderId = orderIds[i];
      const shopId = await contracts.order.methods
        .getOrderShopId(orderId)
        .call();
      const riderId = await contracts.order.methods
        .getOrderRiderId(orderId)
        .call();
      const totalPrice = await contracts.order.methods
        .getOrderTotalPrice(orderId)
        .call();
      const goods = await contracts.order.methods
        .getOrderGoodsIds(orderId)
        .call();
      const stateTimes = await contracts.order.methods
        .getOrderStateTimes(orderId)
        .call();
      const state = await contracts.order.methods
        .getOrderState(orderId)
        .call();
      const cancelSigns = await contracts.order.methods
        .getOrderCancelSign(orderId)
        .call();
      
      ordersCopy.push({
        orderId: orderId,
        shopId: shopId,
        riderId: riderId,
        totalPrice: totalPrice,
        goods: goods,
        stateTimes: stateTimes,
        currentState: state,
        cancelSigns: cancelSigns
      });
    }

    setUserOrders(ordersCopy);
  }

  const getUploadedReviews = async () => {
    const uploadedReviewIds = await contracts.user.methods
      .getUserUploadedReviews(accounts[0])
      .call();
    
    console.log(uploadedReviewIds);

    let uploadedReviewsCopy = [];
    for(let reviewId of uploadedReviewIds) {
      const to = await contracts.review.methods
        .getReviewToUserType(reviewId)
        .call();
      const orderId = await contracts.review.methods
        .getReviewOrderId(reviewId)
        .call();
      const score = await contracts.review.methods
        .getReviewStars(reviewId)
        .call();
      const cancelled = await contracts.review.methods
        .getReviewCancelled(reviewId)
        .call();

      uploadedReviewsCopy.push({
        reviewId,
        to,
        orderId,
        score,
        cancelled });
    }

    setUploadedReviews(uploadedReviewsCopy);
  }

  const clickSignHandle = async (_orderId) => {
    try {
      await contracts.order.methods
        .signOrder(_orderId)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getOrders(accounts[0]);
  }

  const clickShopReviewHandle = async (_shopId) => { 
    console.log(_shopId);

    const reviewIds = await contracts.shop.methods
      .getShopReviews(_shopId)
      .call();
    let reviewsTemp = [];
    for(let rId of reviewIds) {
      const reviewer = await contracts.review.methods
        .getReviewFromUserType(rId)
        .call();
      const stars = await contracts.review.methods
        .getReviewStars(rId)
        .call();
      
      reviewsTemp.push({reviewer, stars});
    }
    
    setOpenShopReviews({open: true, reviews: reviewsTemp}); 
  }

  const clickCloseShopReviewHandle = async () => { setOpenShopReviews({open: false, reviews: []}); }

  const clickCancelOrderHandle = async (_orderId) => {
    try {
      await contracts.order.methods
        .cancelOrderSign(_orderId)
        .send({from: accounts[0]});

        await getOrders(accounts[0]);
      } catch(e) {
      alert(e);
    }
  }

  const clickOpenUploadReviewHandle = async (_orderId) => {
    const reviewedToRider = await contracts.review.methods
      .getReviewed(_orderId, 0, 1)
      .call();
    const reviewedToShop = await contracts.review.methods
      .getReviewed(_orderId, 0, 2)
      .call();
    
    setOpenUploadReview({
      open: true, 
      orderId: _orderId, 
      reviewedToRider,
      reviewedToShop });
  }

  const clickUploadReviewHandle = async () => {
    const target = document.getElementById("selectReviewTarget").value;
    const score = document.getElementById("selectReviewScore").value;

    console.log(target, score, openUploadReview.orderId);

    try {
      if(score < 1 || score > 10 || target < 0 || target > 2) throw "invalid value";
      
      await contracts.review.methods
        .uploadReview(target, openUploadReview.orderId, score)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }
    
    await getOrders(accounts[0]);
    await getUploadedReviews();
    setOpenUploadReview({
      open: false,
      orderId: 0,
      reviewedToRider: false,
      reviewedToShop: false });
  }

  const clickCloseUploadReviewHandle = async () => {
    setOpenUploadReview({
      open: false,
      orderId: 0,
      reviewedToRider: false,
      reviewedToShop: false });
  }

  const clickCancelReviewHandle = async (_reviewId) => {
    try {
      await contracts.review.methods
        .cancelReview(_reviewId)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getUploadedReviews();
  }

  useEffect(() => {
    const init = async () => {
      try {
        await getOrders(accounts[0]);
        await getUploadedReviews();
      } catch(e) {
        alert(e);
      }
    }
    init();
  }, [accounts]);
  
  return(
    <main className="container-fluid row mt-md-3">

      <div className="col-sm-3 first-col">
        <div className="card border-light" style={{maxWidth: "30rem"}}>
          <div className="card-header">User Info</div>
          <div>
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Address</th>
                  <td>{String(accounts[0]).substr(0, 10)}...</td>
                </tr>
                <tr>
                  <th scope="row">ID</th>
                  <td>{userInfo.infos.id}</td>
                </tr>
                <tr>
                  <th scope="row">Home</th>
                  <td>{userInfo.infos.homeAddress}</td>
                </tr>
                <tr>
                  <th scope="row">Reputation</th>
                  <td>{userInfo.infos.reputation}</td>
                </tr>
                <tr>
                  <th scope="row">Rank</th>
                  <td>{userInfo.infos.rank}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="card border-light" style={{maxWidth: "20rem"}}>
            <div className="card-header">Wallet Balances</div>
            <div>
              <table className="table">
                <tbody>
                  <tr>
                    <th scope="row">KRW</th>
                    <td>{userInfo.balances.KRW}</td>
                  </tr>
                  <tr>
                    <th scope="row">KNG</th>
                    <td>{userInfo.balances.KNG}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>          
        </div>
      </div>

      <div className="col-md-8" style={{textAlign: "center"}}>
        <div>
          <legend id="myOrdersToggler" style={{cursor: "pointer"}}>My Orders</legend>
          <UncontrolledCollapse toggler="#myOrdersToggler">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Order ID</th>
                  <th scope="col">Shop</th>
                  <th scope="col">Assigned Rider</th>
                  <th scope="col">Total Price</th> 
                  <th scope="col">Goods</th>
                  <th scope="col">State Changed Time</th>
                  <th scope="col">State</th>
                  <th scope="col">Sign</th>
                  <th scope="col">Reviews</th>
                  <th scope="col">Cancel</th>
                </tr>
              </thead>
              <tbody>
                {
                userOrders.map((order, idx) => (
                  <tr key={idx}>
                    <td>{order.orderId}</td>
                    <td>{order.shopId}</td>
                    <td>{parseInt(order.riderId) || "not assigned"}</td>
                    <td>{order.totalPrice} KRW</td>
                    <td>{String(order.goods)}</td>
                    <td>{new Date(order.stateTimes[0] * 1000).toLocaleString()}</td>
                    <td>{OrderState[order.currentState]}</td>
                    <td><button 
                      className="btn btn-primary btn-sm" 
                      onClick={() => clickSignHandle(order.orderId)}
                      disabled={order.currentState !== '3' ? true : false}
                    >Sign</button></td>
                    <td><button className="btn btn-primary btn-sm" onClick={() => clickShopReviewHandle(order.shopId)}>See Review</button></td>
                    <td>
                    { 
                    order.cancelSigns[0] == 1 ? ( "Signed" ) : (
                      <button 
                        className="btn btn-primary btn-sm" 
                        onClick={() => clickCancelOrderHandle(order.orderId)}
                        disabled={(order.currentState != 0 && order.currentState != 1) ? true : false}
                      >cancel</button> )
                    }
                    </td>
                    {
                    order.currentState == 4 && 
                      (<td><button className="btn btn-primary btn-sm" onClick={() => clickOpenUploadReviewHandle(order.orderId)}>Upload Review</button></td>)
                    }
                  </tr> ))
                }
              </tbody>
            </table>
          </UncontrolledCollapse>
          <Modal isOpen={openShopReviews.open}>
            <ModalHeader>Shop Reviews</ModalHeader>
            <ModalBody>
              <table className="table" style={{textAlign: "center"}}>
                <thead>
                  <tr>
                    <th>Reviewer</th>
                    <th>Stars</th>
                  </tr>
                </thead>
                <tbody>
                  {
                  openShopReviews.reviews.map((review, idx) => (
                    <tr key={idx}>
                      <td>{UserType[review.reviewer]}</td>
                      <td>{review.stars}</td>
                      <td><button className="btn btn-primary">Detail</button></td>
                    </tr>
                  ))
                  }
                </tbody>
              </table>
            </ModalBody>
            <ModalFooter>
              <button className="btn btn-secondary" onClick={clickCloseShopReviewHandle}>Close</button>
            </ModalFooter>
          </Modal>
          <Modal isOpen={openUploadReview.open}>
            <ModalHeader>Upload Review</ModalHeader>
            <ModalBody>
              <div>
                <legend>To</legend>
                <select className="form-control" id="selectReviewTarget">
                  <option style={{fontWeight: "bold"}} value={-1}>Choose Target User</option>
                  <option value={1} disabled={openUploadReview.reviewedToRider}>Rider</option>
                  <option value={2} disabled={openUploadReview.reviewedToShop}>Shop</option>
                </select>
              </div>
              <div>
                <legend>Score</legend>
                <select className="form-control" id="selectReviewScore">
                  <option style={{fontWeight: "bold"}} value={-1}>Choose Score</option>
                  {
                  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((score, idx) => (<option key={idx} value={score}>{score}</option>))
                  }
                </select>
              </div>
            </ModalBody>
            <ModalFooter>
            {
              openUploadReview.reviewedToRider && openUploadReview.reviewedToShop ? (
                <>You Already Uploaded Reviews</>
              ) : (
              <button className="btn btn-primary" onClick={() => clickUploadReviewHandle()}>Upload</button> )
              }
              <button className="btn btn-secondary" onClick={() => clickCloseUploadReviewHandle()}>Close</button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <legend id="uploadedReviewsToggler" style={{cursor: "pointer"}}>My Uploaded Reviews</legend>
          <UncontrolledCollapse toggler="#uploadedReviewsToggler">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Review ID</th>
                  <th scope="col">To</th>
                  <th scope="col">Order ID</th>
                  <th scope="col">Stars</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
              {
                uploadedReviews.map((review, idx) => (
                  <tr key={idx}>
                    <td>{review.reviewId}</td>
                    <td>{UserType[review.to]}</td>
                    <td>{review.orderId}</td>
                    <td>{review.score}</td>
                    <td>
                    {
                      review.cancelled ? "Cancelled" : <button className="btn btn-danger btn-sm" onClick={() => clickCancelReviewHandle(review.reviewId)}>Cancel</button>
                    }
                    </td>
                  </tr>
                ))
              }
              </tbody>
            </table>
          </UncontrolledCollapse>
        </div>
        <div>
          <legend id="receivedReviewToggler" style={{cursor: "pointer"}}>Received Review</legend>
          <UncontrolledCollapse toggler="#receivedReviewToggler">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Review ID</th>
                  <th scope="col">From</th>
                  <th scope="col">order ID</th>
                  <th scope="col">Stars</th>
                  <th scope="col"></th>
                </tr>
              </thead>
            </table>
          </UncontrolledCollapse>
        </div>
      </div>

    </main>
  )
}

export default MyPage;