import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route } from "react-router-dom";

import Web3 from 'web3';

import App from "./App.js";
import Admins from "./contracts/Admins.json";
import Kng from "./contracts/Kng.json";
import Krw from "./contracts/Krw.json";
import Wallet from "./contracts/Wallet.json";
import AppDatas from "./contracts/AppDatas.json";
import DeliveryAppOrder from "./contracts/DeliveryAppOrder.json";
import DeliveryAppRider from "./contracts/DeliveryAppRider.json";
import DeliveryAppReview from "./contracts/DeliveryAppReview.json";
import DeliveryAppShop from "./contracts/DeliveryAppShop.json";
import DeliveryAppUser from "./contracts/DeliveryAppUser.json";
import DeliveryAppVoting from "./contracts/DeliveryAppVoting.json";

const BROWSEPATH = "/delivery";

function LoadingContainer() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState([]);
  const [contracts, setContracts] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      // Searching for modern dapp browser
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } 
      // Legacy dapp browser
      else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } 
      // Develop console host
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
    
      const admins = new web3.eth.Contract(Admins.abi, "0xC4ba325907333068130ceF113d329395f51ae925");
      const kng = new web3.eth.Contract(Kng.abi, "0x43a51E51e0505dF0CB1D410D88aA39E437d1ffE5");
      const krw = new web3.eth.Contract(Krw.abi, "0x49f679895E12E6660d705E0C19934857cB22a17B");
      const wallet = new web3.eth.Contract(Wallet.abi, "0x4595f39F76645eABf001114aE49Df4D52742Bbe3"); 
      const appdatas = new web3.eth.Contract(AppDatas.abi, "0xcdc119756Fe58981921268c54dB92B75AD8a6D48");
      const order = new web3.eth.Contract(DeliveryAppOrder.abi, "0x074b48eFf66Bba22389Ac0ebEfc9231E766fb910");
      const rider = new web3.eth.Contract(DeliveryAppRider.abi, "0x82a2EDD9Cdf5a518715677E22F791e373Aca29C5");
      const review = new web3.eth.Contract(DeliveryAppReview.abi, "0x3Db613B16D03A733dBFA18dEA0b53499787900C0");
      const shop = new web3.eth.Contract(DeliveryAppShop.abi, "0x19694F5b72A96D811F64c502bD4155676F3310e3");
      const user = new web3.eth.Contract(DeliveryAppUser.abi, "0xaF27415669c2b754068e3488FeAA88C413dedD21");
      const vote = new web3.eth.Contract(DeliveryAppVoting.abi, "0xE90D59F6c365a8fc7bF431a5f9FFE10A1B3E6c53");
      
      const contracts = { 
        admins,
        kng, krw, wallet,
        appdatas, order, rider, review, shop, user, vote }

      setWeb3(web3);
      setContracts(contracts);
    }
    init();
    
    window.ethereum.on('accountsChanged', accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof web3 !== 'undefined' 
      && typeof contracts !== 'undefined'
      && accounts.length > 0
    );
  }

  if (!isReady()) {
    return (
    <BrowserRouter>
      <Route path={BROWSEPATH}>
        <div>Loading...</div>;
      </Route>
    </BrowserRouter> );
  }

  return (
        <App web3={web3} accounts={accounts} contracts={contracts} />
  );

}

export default LoadingContainer;